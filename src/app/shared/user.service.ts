import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, tap } from 'rxjs';
import { UserEventService } from './user-event.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private apiUrl = 'https://6582ccea02f747c8367a3ff7.mockapi.io/users';
    

    constructor(private http: HttpClient, private userEventService: UserEventService
        ) { }

    getUsers(): Observable<any> {
        return this.http.get<any>(this.apiUrl);
    }

    getUserById(id: number): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.http.get<any>(url);
    }

    createUser(user: any): Observable<any> {
        return this.http.post<any>(this.apiUrl, user).pipe(
            tap(() => this.userEventService.emitUserUpdated())
          );;
    }

    updateUser(id: number, user: any): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.http.put<any>(url, user).pipe(
            tap(() => this.userEventService.emitUserUpdated())
          );;
    }

    deleteUser(id: number): Observable<any> {
        const url = `${this.apiUrl}/${id}`;
        return this.http.delete<any>(url);
    }

    
}
