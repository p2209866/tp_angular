import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserEventService {
  private userUpdatedSubject = new Subject<void>();
  private userCreatedSubject = new Subject<void>();
  
  userUpdated$ = this.userUpdatedSubject.asObservable();
  userCreated$ = this.userCreatedSubject.asObservable();

  emitUserUpdated() {
    this.userUpdatedSubject.next();
  }
  
  emitUserCreated() {
    this.userCreatedSubject.next();
  }
}