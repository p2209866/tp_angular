import { Component, Inject } from '@angular/core';
import { User } from '../user.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent {
  updateUserForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User,
    private userService: UserService,
    private fb: FormBuilder,
    
  ) { 
    this.updateUserForm = this.fb.group({
      name: [this.user.name || '', [Validators.required]],
      occupation: [this.user.occupation || '', [Validators.required]],
      email: [this.user.email || '', [Validators.required, Validators.email]],
    });
  }

  onSubmit() {
    if (this.updateUserForm.valid) {
      const updatedUser = { ...this.user, ...this.updateUserForm.value };
      this.userService.updateUser(this.user.id, updatedUser).subscribe(
        (response) => {
          console.log(JSON.stringify(response, null, 2))
          this.dialogRef.close();
          this.user = response;
        },
        (error) => {
          if (error.status === 500) {
            alert('Une erreur interne du serveur s\'est produite. Veuillez réessayer plus tard.');
          } else {
            alert('Une erreur s\'est produite lors de la création de l\'utilisateur.');
          }
        }
      );
    }
  }
}

  
