import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from 'src/app/shared/user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
  
  addUserForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public user: User,
    private userService: UserService,
    private fb: FormBuilder,
    
  ) { 
    this.addUserForm = this.fb.group({
      name: ['', [Validators.required]],
      occupation: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      bio: ['', [Validators.required]],
    });
  }

  onSubmit(): void {
    if (this.addUserForm.valid) {
      const newUser = this.addUserForm.value;
      this.userService.createUser(newUser).subscribe(
        (response) => {
          console.log(JSON.stringify(response, null, 2))
          this.dialogRef.close();
          this.user = response;
        },
        (error) => {
          if (error.status === 500) {
            alert('Une erreur interne du serveur s\'est produite. Veuillez réessayer plus tard.');
          } else {
            alert('Une erreur s\'est produite lors de la création de l\'utilisateur.');
          }
        }
      );
    }
  }
}
