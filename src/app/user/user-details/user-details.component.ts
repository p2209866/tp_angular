import { Component, Input } from '@angular/core';
import { User } from '../user.model';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent {
  

  constructor(@Inject(MAT_DIALOG_DATA) public user: User,private dialogRef: MatDialogRef<UserDetailsComponent>) {
  }
  
  goBackToList(): void {
    this.dialogRef.close();
  }

}
