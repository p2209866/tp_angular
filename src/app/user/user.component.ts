import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from './user.model';
import { UserService } from '../shared/user.service';
import { MatDialog } from '@angular/material/dialog';
import { UserDetailsComponent } from '../user/user-details/user-details.component'
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserEventService } from '../shared/user-event.service';
import { Subscription } from 'rxjs';
import { AddUserComponent } from './add-user/add-user.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  
  dataSource = new MatTableDataSource<User>([]);
  displayedColumns: string[] = ['name', 'occupation', 'email', 'action'];
  private userUpdatedSubscription: Subscription;
  private userCreatedSubscription: Subscription;

  constructor(private userService: UserService, private dialog: MatDialog, private userEventService: UserEventService,) {
    this.userService.getUsers().subscribe((users: User[]) => {
      this.dataSource.data = users;
    });
    this.userUpdatedSubscription = this.userEventService.userUpdated$.subscribe(() => {
      this.refreshList();
    });
    this.userCreatedSubscription = this.userEventService.userCreated$.subscribe(() => {
      this.refreshList();
    });
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    
    if (!filterValue) {
      this.refreshList();
      return;
    }

    const normalizedFilterValue = filterValue.toLowerCase();
    this.dataSource.data = this.dataSource.data.filter((user: User) => 
      user.name.toLowerCase().includes(normalizedFilterValue) || 
      user.email.toLowerCase().includes(normalizedFilterValue) || 
      user.occupation.toLowerCase().includes(normalizedFilterValue)
    );
  }
  

  ngOnInit(): void {
    this.userService.getUsers().subscribe((users: User[]) => {
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
    });
  }
  
  refreshList() {
    this.userService.getUsers().subscribe((users: User[]) => {
      this.dataSource.data = users;
    });
  }

  showDetails(user: User) {
    const dialogRef = this.dialog.open(UserDetailsComponent, {
      width: '400px',
      data: user
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  
deleteUser(user: User) {
  this.userService.deleteUser(user.id).subscribe(() => {
    this.dataSource.data = this.dataSource.data.filter(u => u.id !== user.id);
  });
}

editUser(user: User) {
  const dialogRef = this.dialog.open(EditUserComponent, {
    width: '400px',
    data: user
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}

addUser() {
  const dialogRef = this.dialog.open(AddUserComponent, {
    width: '400px',
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
  });
}
}

